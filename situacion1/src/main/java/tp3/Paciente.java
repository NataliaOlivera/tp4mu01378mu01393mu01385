package tp3;

import java.math.BigDecimal;

class Paciente {
    private String codigo;
    private String alias;
    private String especie;
    private String raza;
    private String colorPelo;
    private String fechaNacimiento;
    private BigDecimal pesoProm;
    private BigDecimal pesoActual;
    private BigDecimal[] pesos = new BigDecimal[10];
    private HistoriaClinica historiaClinica = new HistoriaClinica("1.4.17");
    private CalendarioVacunacion calendarioVacunacion = new CalendarioVacunacion();

    public Paciente(String codigo, String alias, String especie, String raza, String colorPelo,
            String fechaNacimiento) {
        setCodigo(codigo);
        setAlias(alias);
        setEspecie(especie);
        setRaza(raza);
        setColorPelo(colorPelo);
        setFechaNacimiento(fechaNacimiento);
    }

    public void VisualizarPaciente(){
        System.out.println("Datos Del Paciente:");
        System.out.println("Codigo:" +getCodigo());
        System.out.println("Alias:" +getAlias());
        System.out.println("Especie:" +getEspecie());
        System.out.println("Raza:" +getRaza());
        System.out.println("Color De Pelo:" +getColorPelo());
        System.out.println("Fecha De Nacimiento:" +getFechaNacimiento());
        System.out.println("Peso Actual:" +getPesoActual());
    }

    public CalendarioVacunacion getCalendarioVacunacion() {
        return calendarioVacunacion;
    }

    public void setCalendarioVacunacion(CalendarioVacunacion calendarioVacunacion) {
        this.calendarioVacunacion = calendarioVacunacion;
    }

    public HistoriaClinica getHistoriaClinica() {
        return historiaClinica;
    }

    public void setHistoriaClinica(HistoriaClinica historiaClinica) {
        this.historiaClinica = historiaClinica;
    }

    public Paciente(BigDecimal pesoActual) {
        setPesoActual(pesoActual);
    }

    public Paciente(HistoriaClinica historiaClinica, CalendarioVacunacion calendarioVacunacion) {
        this.setHistoriaClinica(historiaClinica);
        this.setCalendarioVacunacion(calendarioVacunacion);
    }

    public String getCodigo(){
        return codigo;
    }

    public void setCodigo(String codigo){
        this.codigo=codigo;
    }

    public String getAlias(){
        return alias;
    }

    public void setAlias(String alias){
        this.alias=alias;
    }

    public String getEspecie(){
        return especie;
    }

    public void setEspecie(String especie){
        this.especie=especie;
    }

    public String getRaza(){
        return raza;
    }

    public void setRaza(String raza){
        this.raza=raza;
    }

    public String getColorPelo(){
        return colorPelo;
    }

    public void setColorPelo(String colorPelo){
        this.colorPelo=colorPelo;
    }

    public String getFechaNacimiento(){
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento){
        this.fechaNacimiento=fechaNacimiento;
    }

    public BigDecimal getPesoProm() {
        BigDecimal numerador=new BigDecimal(0);
        for(int i=0;i<10;i++)
        {
            numerador=numerador.add(pesos[i]);
        }
        pesoProm=numerador.divide(new BigDecimal(10));
        return pesoProm;
    }

    public void vectorPesos(BigDecimal pesoActual) {
        for(int i=0;i<10;i++)
        {
            if(pesos[i]==null)
            {
                pesos[i]=pesoActual;
                break;
            }
            else
            {
                System.out.println("vector lleno");
            }
        }
        
    }

    public BigDecimal getPesoActual() {
        return pesoActual;
    }

    public void setPesoActual(BigDecimal pesoActual) {
        this.pesoActual = pesoActual;
        vectorPesos(pesoActual);
    }
}