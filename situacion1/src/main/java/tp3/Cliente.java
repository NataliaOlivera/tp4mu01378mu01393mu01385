package tp3;

class Cliente {
    private String codigo;
    private String apellidoFlia;
    private int numCuentaBancaria;
    private String direccion;
    private String telefono;
    private Persona[] familia = new Persona[10];//Reemplazarlo por un ArrayList 

    public Cliente(String codigo, String apellidoFlia, int numCuentaBancaria, String direccion, String telefono){
        setCodigo(codigo);
        setApellidoFlia(apellidoFlia);
        setNumCuentaBancaria(numCuentaBancaria);
        setDireccion(direccion);
        setTelefono(telefono);
    }

    
    public Cliente(){

    }
    public void verCliente() {
            System.out.println("EL cliente es "+getCodigo()+" "+getApellidoFlia()+" "+getNumCuentaBancaria()+" "+getDireccion()+" "+getTelefono());
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getNumCuentaBancaria() {
        return numCuentaBancaria;
    }

    public void setNumCuentaBancaria(int numCuentaBancaria) {
        this.numCuentaBancaria = numCuentaBancaria;
    }

    public String getApellidoFlia() {
        return apellidoFlia;
    }

    public void setApellidoFlia(String apellidoFlia) {
        this.apellidoFlia = apellidoFlia;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void agregarFamiliar(Persona familiar) {
        for(int i=0;i<10;i++){
            if(familia[i]==null){
                familia[i]=familiar;
            }
            else{
                System.out.println("El grupo familiar esta lleno");
            }
        }
    }

}