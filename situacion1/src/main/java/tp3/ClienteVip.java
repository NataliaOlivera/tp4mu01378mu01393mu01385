package tp3;

import java.math.BigDecimal;

public class ClienteVip extends Cliente {

    private Paciente[] paciente = new Paciente[10];
    private BigDecimal bonificacion = new BigDecimal(0.10);

    public ClienteVip(String codigo, String apellidoFlia, int numCuentaBancaria, String direccion, String telefono){
        super(codigo, apellidoFlia, numCuentaBancaria, direccion, telefono);
    }

    public Paciente[] getPaciente() {
        return paciente;
    }


}