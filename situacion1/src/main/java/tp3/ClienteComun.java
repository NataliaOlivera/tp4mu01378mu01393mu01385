package tp3;

import java.math.BigDecimal;

public class ClienteComun extends Cliente{

    private Paciente[] paciente = new Paciente[2];
    private BigDecimal bonificacion = new BigDecimal(0.05);

    public ClienteComun(String codigo, String apellidoFlia, int numCuentaBancaria, String direccion, String telefono){
        super(codigo, apellidoFlia, numCuentaBancaria, direccion, telefono);
    }

    public Paciente[] getPaciente() {
        return paciente;
    }


}