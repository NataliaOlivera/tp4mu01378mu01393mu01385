
package tp3;


public class Enfermedad {
    public int codigo;
    public String descripcion;
   

    //Constructor
    public Enfermedad(int codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }
    
    //Set y get
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }
    //
        public void MostrarEnfermedades(Enfermedad enfermedades){
        System.out.println("---Listado de enfermedades---");
        System.out.println(enfermedades);
	}
    
}
