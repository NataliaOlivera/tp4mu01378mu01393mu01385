
package tp3;

import java.util.ArrayList;

public class HistoriaClinica {
    
    private String fechaUltimaVisita;
    private ArrayList<Enfermedad> enfermedades;
    
    public HistoriaClinica(String fechaUltimaVisita){
        setfechaUltimaVisita(fechaUltimaVisita);
    }

    public String getfechaUltimaVisita() {
        return fechaUltimaVisita;
    }

    public void setfechaUltimaVisita(String fechaUltimaVisita) {
        this.fechaUltimaVisita = fechaUltimaVisita;
    }

    public ArrayList getEnfermedades() {
        return enfermedades;
    }

    public void setEnfermedades(ArrayList enfermedades) {
        this.enfermedades = enfermedades;
    }
    
    

    
    public void VerHistoriaClinica(){
        System.out.println("Historia Clinica:");
        System.out.println("Fecha de la Ultima Visita:" +getfechaUltimaVisita());    
        for(Enfermedad enfermedades : enfermedades){
        enfermedades.MostrarEnfermedades(enfermedades);
        }
    }
   
}