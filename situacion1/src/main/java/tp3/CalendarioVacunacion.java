
package tp3;


public class CalendarioVacunacion {
    
    public String fechaVacuna;
    public String enfermedadVacuna;

    public CalendarioVacunacion(String enfermedadVacuna, String fechaVacuna){
        setEnfermedadVacuna(enfermedadVacuna);
        setFechaVacuna(fechaVacuna);
    }

    public CalendarioVacunacion(){
    }
    
    public void VerCalendarioVacunacion(){
        System.out.println("Datos de vacunacion: ");
        System.out.println("Enfermedad "+getEnfermedadVacuna());
        System.out.println("Fecha: "+getFechaVacuna());
    }

    public String getEnfermedadVacuna() {
        return enfermedadVacuna;
    }

    public void setEnfermedadVacuna(String enfermedadVacuna) {
        this.enfermedadVacuna = enfermedadVacuna;
    }

    public String getFechaVacuna() {
        return fechaVacuna;
    }

    public void setFechaVacuna(String fechaVacuna) {
        this.fechaVacuna = fechaVacuna;
    }

}