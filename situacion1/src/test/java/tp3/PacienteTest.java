package tp3;

import org.junit.Test;
import static org.junit.Assert.*;

import java.math.BigDecimal;

public class PacienteTest{
     @Test
     public void caracteristicasDelPaciente(){
        Paciente paciente=new Paciente("1234","Pequeño","Perro","Pitbull","Marron","150319");
        assertEquals(paciente.getCodigo(),"1234");
        assertEquals(paciente.getAlias(),"Pequeño");
        assertEquals(paciente.getEspecie(),"Perro");
        assertEquals(paciente.getRaza(),"Pitbull");
        assertEquals(paciente.getColorPelo(),"Marron");
        assertEquals(paciente.getFechaNacimiento(),"150319");
     }

     @Test
     public void calculandoPesoPromedioDelPaciente(){
      Paciente paciente=new Paciente(new BigDecimal(15.00));
      paciente.setPesoActual(new BigDecimal(15.00));
      paciente.setPesoActual(new BigDecimal(15.00));
      paciente.setPesoActual(new BigDecimal(15.00));
      paciente.setPesoActual(new BigDecimal(15.00));
      paciente.setPesoActual(new BigDecimal(15.00));
      paciente.setPesoActual(new BigDecimal(15.00));
      paciente.setPesoActual(new BigDecimal(15.00));
      paciente.setPesoActual(new BigDecimal(15.00));
      paciente.setPesoActual(new BigDecimal(15.00));

      assertEquals(paciente.getPesoActual(),new BigDecimal (15.00));
      assertEquals(paciente.getPesoProm(),new BigDecimal(15.00));

     }


}