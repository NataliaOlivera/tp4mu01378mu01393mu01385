package tp3;

import org.junit.Test;
import static org.junit.Assert.*;

public class CalendarioVacunacionTest {
    @Test
    public void Calendario(){
        CalendarioVacunacion vacunacion = new CalendarioVacunacion("enfermedad", "11/07/2018");    
        assertEquals(vacunacion.getEnfermedadVacuna(),"enfermedad");
        assertEquals(vacunacion.getFechaVacuna(),"11/07/2018");
    }
    }