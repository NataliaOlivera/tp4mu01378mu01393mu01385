package situacion2;

import org.junit.Test;
import static org.junit.Assert.*;

import java.math.BigDecimal;

public class LimpiadorTest{
    @Test
    
    public void sueldoFinalQueTieneElLimpiador(){
        HorasExtras horasExtras=new HorasExtras(new BigDecimal(7.50), "15Agos2019", "17Agos2019");
        Limpiador limpiador=new Limpiador(new BigDecimal(550.00), horasExtras);    
        assertEquals(limpiador.getAdicional(),new BigDecimal(550.00));
        limpiador.calSuel(limpiador.getCantidad());
        assertEquals(limpiador.getSuelTotal(),new BigDecimal(12553.75));
    }
}