package situacion2;

import org.junit.Test;
import static org.junit.Assert.*;

import java.math.BigDecimal;

public class AdministrativoTest{
    @Test

    public void sueldoDelAdministrativo(){
        Administrativo administrativo= new Administrativo(new BigDecimal(1200.00));
        assertEquals(administrativo.getAdicional(),new BigDecimal(1200.00));
        assertEquals(administrativo.getSuelTotal(),new BigDecimal(16200.00));
    }
}