package situacion2;

import org.junit.Test;
import static org.junit.Assert.*;

public class ContratoEmpleadoTest {
    @Test
    public void DatosEmpleado(){
        ContratoEmpleado contrato = new ContratoEmpleado("03/12/2010",5,3452);
        assertEquals(contrato.getFecha(),"03/12/2010");
        assertEquals(contrato.getDuracion(),5);
        assertEquals(contrato.getCodigo(),3452);
    }
}
