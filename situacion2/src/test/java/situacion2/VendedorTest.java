package situacion2;

import org.junit.Test;
import static org.junit.Assert.*;
import java.math.BigDecimal;

public class VendedorTest{

    @Test
    public void CrearVendedor(){
        ContratoVendedor contrato= new ContratoVendedor("Jose","Calle falsa 123","12/12/97",12,04231);
        Vendedor vendedor= new Vendedor(new BigDecimal(10000), new BigDecimal(300),new BigDecimal(4), contrato);
        assertEquals(vendedor.getSuelBas(), new BigDecimal(10000));
        assertEquals(vendedor.getAdicional(), new BigDecimal(300));
        assertEquals(vendedor.getNumContrato(), new BigDecimal(4));
        assertEquals(vendedor.getContrato(), contrato);
    }

}