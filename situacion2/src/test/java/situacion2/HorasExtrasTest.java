package situacion2;

import org.junit.Test;
import static org.junit.Assert.*;

import java.math.BigDecimal;

public class HorasExtrasTest{
    @Test   
    
    public void informacionSobreHorasExtras(){
        HorasExtras horasExtras = new HorasExtras(new BigDecimal(7.50), "15Agos2019", "17Agos2019");
        assertEquals(horasExtras.getCantidad(),new BigDecimal (7.50));
        assertEquals(horasExtras.getFechaInicio(), "15Agos2019");
        assertEquals(horasExtras.getFechaFinal(), "17Agos2019");
    }
}