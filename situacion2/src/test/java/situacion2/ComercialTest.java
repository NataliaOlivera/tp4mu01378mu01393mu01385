package situacion2;

import org.junit.Test;
import static org.junit.Assert.*;
import java.math.BigDecimal;

public class ComercialTest{

    @Test
    public void CrearComercial(){
        
        Comercial comercial= new Comercial(new BigDecimal(20000), new BigDecimal(200),new BigDecimal(2));
        assertEquals(comercial.getSuelBas(), new BigDecimal(20000));
        assertEquals(comercial.getAdicional(), new BigDecimal(200));
        assertEquals(comercial.getNumContrato(), new BigDecimal(2));
    }

 
   




}