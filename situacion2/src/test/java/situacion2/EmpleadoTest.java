
package situacion2;

import org.junit.Test;
import static org.junit.Assert.*;

public class EmpleadoTest {
    @Test
    public void DatosEmpleado(){
        Empleado empleado = new Empleado("Martin","40926786","dni","Lopez","Sarmiento 356",1);
        assertEquals(empleado.getNombre(),"Martin");
        assertEquals(empleado.getDocumento(),"40926786");
        assertEquals(empleado.getTipoDoc(),"dni");
        assertEquals(empleado.getApellido(),"Lopez");
        assertEquals(empleado.getDomicilio(),"Sarmiento 356");
        assertEquals(empleado.getBandera(),1);
    }
    
}
