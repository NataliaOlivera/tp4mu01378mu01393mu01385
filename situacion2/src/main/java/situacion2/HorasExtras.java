package situacion2;

import java.math.BigDecimal;

class HorasExtras {
    private BigDecimal cantidad;
    private String fechaInicio;
    private String fechaFinal;

    public HorasExtras(BigDecimal cantidad, String fechaInicio, String fechaFinal){
        setCantidad(cantidad);
        setFechaInicio(fechaInicio);
        setFechaFinal(fechaFinal);
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public String getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(String fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }
 }