package situacion2;

import java.math.BigDecimal;

public class Vendedor extends Comercial {


    private ContratoVendedor contrato;
    //constructor

    public Vendedor(){

    }

    public Vendedor(BigDecimal suelBas,BigDecimal adicional, BigDecimal numContrato, ContratoVendedor contrato){

        super(suelBas,adicional,numContrato);
        setContrato(contrato);

    }

    public ContratoVendedor getContrato() {
        return contrato;
    }

    public void setContrato(ContratoVendedor contrato) {
        this.contrato = contrato;
    }
}