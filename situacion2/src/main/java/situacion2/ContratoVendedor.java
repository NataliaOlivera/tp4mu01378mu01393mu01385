package situacion2;


public class ContratoVendedor {

	private String nombCliente;
	private String direccion;
	private String fechaInicio;
	private int duracion;
	private int codigo;
	private int Altabaja;

	// constructor

	public ContratoVendedor(String nombCliente, String direccion, String fechaInicio, int duracion, int codigo) {

        setNombCliente(nombCliente);
        setDireccion(direccion);
        setFechaInicio(fechaInicio);
        setDuracion(duracion);
        setCodigo(codigo);
    }

    public void VerContrato(){
		System.out.println("Nombre:"+getNombCliente());
		System.out.println("\nDireccion:"+getDireccion());
		System.out.println("\nFecha de inicio:"+getFechaInicio());
		System.out.println("\nDuracion:"+getDuracion());
		System.out.println("\nCodigo:"+getCodigo());
	}
	public void AltaContrato(){
		int altabaja=1;
		setAltabaja(altabaja);
	}
	public void BajaContrato(){
		int altabaja=0;
		setAltabaja(altabaja);
	}

	public String getNombCliente() {
		return nombCliente;
	}

	public void setNombCliente(String nombCliente) {
		this.nombCliente = nombCliente;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public int getAltabaja() {
		return Altabaja;
	}

	public void setAltabaja(int altabaja) {
		Altabaja = altabaja;
	}



}