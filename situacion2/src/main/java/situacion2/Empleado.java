
package situacion2;

public class Empleado {
    
    private String tipoDocumento;
    private String documento;
    private String nombre;
    private String apellido;
    private String domicilio;
    private int bandera;
    private String estado;
    private ContratoEmpleado contrato;
   

     
    public Empleado(String nombre,String documento, String tipoDocumento, String apellido, String domicilio,int bandera){
           this.contrato = new ContratoEmpleado("03/12/2010",5,3452);
           setNombre(nombre);
           setDocumento(documento);
           setTipoDoc(tipoDocumento);
           setApellido(apellido);
           setDomicilio(domicilio);
           setBandera(bandera);
       
        }
    public Empleado(){
        this.contrato = new ContratoEmpleado();
    }
    
    public void verEmpleado() {
       System.out.println("Datos Empleado");
       System.out.println("Apellido y nombre: "+getApellido()+" "+getNombre());
       System.out.println("Domicilio: "+getDomicilio());
       System.out.println("Documento: "+getDocumento());
       Estado(bandera);
       System.out.println("Estado: "+estado);    
       contrato.verContratoEmpleado();
      
    }


    public void setBandera(int bandera) {
        this.bandera = bandera;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }
    public void setNombre(String nombre){
        this.nombre=nombre;
    }
    public void setDocumento(String documento){
        this.documento=documento;
    }
    public void setTipoDoc(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }
    public String getTipoDoc(){
        return tipoDocumento;
    }
    public String getApellido(){
        return apellido;
    }
    public String getDomicilio(){
        return domicilio;
        }
    public String getDocumento(){
        return documento;
    }
    public String getNombre(){
        return nombre;
    }
     public String getEstado() {
        return estado;
    }
    public int getBandera() {
        return bandera;
    }
    
    public String Estado(int bandera){
        if (bandera==1){
            estado = "Activo";
        }
        else{
        estado = "Inactivo";
        }    
        return estado;
        }
}
