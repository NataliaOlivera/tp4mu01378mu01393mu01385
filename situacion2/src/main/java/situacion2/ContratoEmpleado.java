
package situacion2;

public class ContratoEmpleado {
    private String fechaInicio;
    private int duracion;
    private int codigo;
   
    
    public ContratoEmpleado(String fechaInicio,int duracion, int codigo){
        setFecha(fechaInicio);
        setDuracion(duracion);
        setCodigo(codigo);
    }
    
    public ContratoEmpleado(){
    }
    public void verContratoEmpleado(){
       System.out.println("Datos del Contrato");
       System.out.println("Fecha de inicio: "+getFecha());
       System.out.println("Duracion: "+getDuracion());
       System.out.println("Codigo: "+getCodigo());
    }
    
    public void setFecha(String fechaInicio){
            this.fechaInicio=fechaInicio;
        }
    public void setDuracion(int duracion){
            this.duracion=duracion;
        }
    public void setCodigo(int codigo){
            this.codigo = codigo;
        }
 
    public String getFecha(){
            return fechaInicio;
        }
    public int getDuracion(){
            return duracion;
        }
    public int getCodigo(){
            return codigo;
        }
 
}
