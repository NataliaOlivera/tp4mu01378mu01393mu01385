package situacion2;

import java.math.BigDecimal;

class Administrativo extends Empleado{
    private BigDecimal suelBas = new BigDecimal(15000.00);
    private BigDecimal adicional;
    private BigDecimal suelTotal;

    public Administrativo(String nombre,String documento, String tipoDocumento, String apellido, String domicilio,int bandera,BigDecimal adicional) {
        super(nombre, documento, tipoDocumento, apellido, domicilio, bandera);
        setAdicional(adicional);
    }

    public Administrativo( BigDecimal adicional) {
        setAdicional(adicional);
    }

    public BigDecimal getSuelTotal() {
        return suelTotal;
    }

    public void calSuel(BigDecimal adicional) {
        suelTotal=suelBas.add(adicional);
    }

    public BigDecimal getAdicional() {
        return adicional;
    }

    public void setAdicional(BigDecimal adicional) {
        this.adicional = adicional;
        calSuel(adicional);
    }
}