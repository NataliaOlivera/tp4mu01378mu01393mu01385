package situacion2;

import java.math.BigDecimal;

class Limpiador extends Empleado{
    private BigDecimal suelBas=new BigDecimal(12000.00);
    private BigDecimal adicional;
    private HorasExtras horasExtras;
    private BigDecimal suelTotal;

    public Limpiador(String nombre,String documento, String tipoDocumento, String apellido, String domicilio,int bandera,BigDecimal adicional, HorasExtras horasExtras){
        super(nombre, documento, tipoDocumento, apellido, domicilio, bandera);
        setAdicional(adicional);
        setHorasExtras(horasExtras);
    }

    public Limpiador(BigDecimal adicional, HorasExtras horasExtras ){
        setAdicional(adicional);
        setHorasExtras(horasExtras);
    }

    private void setHorasExtras(HorasExtras horasExtras) {
        this.horasExtras=horasExtras;
    }

    public void calSuel(BigDecimal cantidad) {
        suelTotal=suelBas.add(adicional).add(cantidad.multiply(new BigDecimal(0.5)));
    }

    public BigDecimal getSuelTotal(){
        return suelTotal;
    }

    public BigDecimal getAdicional() {
        return adicional;
    }

    public void setAdicional(BigDecimal adicional) {
        this.adicional = adicional;
    }

    public BigDecimal getCantidad() {
        return horasExtras.getCantidad();
    }
}