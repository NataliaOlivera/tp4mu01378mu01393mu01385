package situacion2;

import java.math.BigDecimal;

class Comercial{

    private BigDecimal suelBas= new BigDecimal(15000.00);
    private BigDecimal adicional;
    private BigDecimal numContrato;
    private BigDecimal suelTotal;


    public Comercial(BigDecimal suelBas,BigDecimal adicional, BigDecimal numContrato){
        setSuelBas(suelBas);
        setAdicional(adicional);
        setNumContrato(numContrato);
    }

    public Comercial(){
        
    }

    public BigDecimal calSuel(BigDecimal suelBas, BigDecimal adicional, BigDecimal numContrato) {
        suelTotal=suelBas.add(adicional).add((suelBas.multiply(new BigDecimal(0.1))).multiply(numContrato));
        return suelTotal;
    }

    public void setSuelBas(BigDecimal suelBas){
        this.suelBas= suelBas;
    }
    public BigDecimal getSuelBas(){
        return suelBas;
    }

    public BigDecimal getAdicional() {
        return adicional;
    }

    public void setAdicional(BigDecimal adicional) {
        this.adicional = adicional;
    }

    public BigDecimal getNumContrato() {
        return numContrato;
    }

    public void setNumContrato(BigDecimal numContrato) {
        this.numContrato = numContrato;
    }

    
}